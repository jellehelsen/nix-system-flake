{ config, lib, pkgs, ... }:

{
  services.dunst = {
    enable = true;
    settings = {
      global = {
        font = "Work Sans Regular 12";
        markup = "yes";
        plain_text = "no";
        format = ''
          <b>%a</b> %i %I %p : <big><b>%s</b></big>
          %b'';
        sort = "yes";
        indicate_hidden = "yes";
        alignment = "left";
        vertical_alignment = "center";
        bounce_freq = 0;
        show_age_threshold = 60;
        word_wrap = "yes";
        ignore_newline = "no";
        stack_duplicates = "yes";
        hide_duplicates_count = "no";
        geometry = "500x30-40+45";
        corner_radius = 0;
        shrink = "no";
        transparency = 0;
        idle_threshold = 5;
        show_indicators = "yes";
        line_height = -1;
        separator_height = 5;
        padding = 10;
        horizontal_padding = 10;
        separator_color = "auto";
        dmenu =
          "/etc/profiles/per-user/jelle/bin/rofi -dmenu -p 'Open URL' -theme 'glass/simonvic_dunstActions'";
        browser = "firefox --new-tab";
        icon_position = "left";
        max_icon_size = 60;
        min_icon_size = 10;
        frame_width = 0;
        frame_color = "#N";
      };
      urgency_low = {
        frame_color = "#33333355";
        foreground = "#888a85";
        background = "#191311AA";
        timeout = 8;
      };
      urgency_normal = {
        frame_color = "#5B823455";
        foreground = "#CACACA";
        background = "#191311AA";
        timeout = 8;
      };
      urgency_critical = {
        frame_color = "#B7472A55";
        foreground = "#B7472A";
        background = "#191311AA";
        timeout = 16;
      };
    };
  };
}
