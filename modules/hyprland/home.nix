{ config, lib, pkgs, host, locker, ... }:
let
  touchpad = ''
    touchpad {
      natural_scroll=true
      middle_button_emulation=true
      tap-to-click=true
    }
  '';
  lockCommand = "${locker}/bin/hyprlock";
  rofi = ''
    rofi -show combi -combi-modi 'drun,run,window' -mode 'combi,drun,run'
  '';
  hyprlandConf = ''
    monitor=DP-1,3440x1440,0x0,1
    monitor=eDP-1,1920x1080,3440x0,1
    env = XCURSOR_SIZE,24
    env = LIBVA_DRIVER_NAME,nvidia
    env = XDG_SESSION_TYPE,wayland
    env = GBM_BACKEND,nvidia-drm
    env = __GLX_VENDOR_LIBRARY_NAME,nvidia
    env = WLR_NO_HARDWARE_CURSORS,1
    env = ELECTRON_OZONE_PLATFORM_HINT,auto
    env = NIXOS_OZONE_WL,1

    # cursor {
    #     no_hardware_cursors = true
    # }


    # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
    input {
        kb_layout = us
        kb_variant =
        kb_model =
        kb_options =
        kb_rules =

        follow_mouse = 1

        touchpad {
            natural_scroll = true
        }

        sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
    }

    general {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more

        gaps_in = 5
        gaps_out = 20
        border_size = 2
        col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
        col.inactive_border = rgba(595959aa)

        layout = dwindle
    }

    decoration {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more

        rounding = 10
        # blur = yes
        # blur_size = 3
        # blur_passes = 1
        # blur_new_optimizations = true

        # drop_shadow = yes
        # shadow_range = 4
        # shadow_render_power = 3
        # col.shadow = rgba(1a1a1aee)
    }

    misc {
        mouse_move_enables_dpms = true
        key_press_enables_dpms = true
    }

    animations {
        enabled = yes

        # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

        bezier = myBezier, 0.05, 0.9, 0.1, 1.05

        animation = windows, 1, 7, myBezier
        animation = windowsOut, 1, 7, default, popin 80%
        animation = border, 1, 10, default
        animation = borderangle, 1, 8, default
        animation = fade, 1, 7, default
        animation = workspaces, 1, 6, default
    }

    dwindle {
        # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
        pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        preserve_split = yes # you probably want this
    }

    master {
        # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        # new_is_master = true
    }

    gestures {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        workspace_swipe = true
    }

    # Example per-device config
    # See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
    # device:epic-mouse-v1 {
    #     sensitivity = -0.5
    # }

    workspace = 1, monitor:DP-1
    workspace = 2, monitor:DP-1
    workspace = 3, monitor:DP-1
    workspace = 4, monitor:DP-1
    workspace = 5, monitor:DP-1
    workspace = 6, monitor:DP-1
    workspace = 7, monitor:eDP-1
    workspace = 8, monitor:eDP-1

    # Example windowrule v1
    # windowrule = float, ^(kitty)$
    windowrule = float, ^(Rofi)$
    windowrule = center, ^(Rofi)$
    windowrule = stayfocused, ^(Rofi)$
    # windowrule = forceinput, ^(Rofi)$
    windowrule = workspace:3, ^(Emacs$)
    windowrule = workspace:1, ^(brave-browser)$
    # Example windowrule v2
    # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
    # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
    # windowrulev2 = workspace:3,class:^(Emacs)$


    # See https://wiki.hyprland.org/Configuring/Keywords/ for more
    $mainMod = SUPER

    # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
    bind = $mainMod, Q, exec, kitty
    bind = $mainMod, return, exec, alacritty
    bind = $mainMod, C, killactive,
    bind = $mainMod SHIFT, Q, exec, [float;center] ~/.config/rofi/exit.sh
    bind = $mainMod, E, exec, pcmanfm
    bind = $mainMod, V, togglefloating,
    bind = CTRL, space, exec, [float;center] ${rofi}
    bind = $mainMod, P, pseudo, # dwindle
    bind = $mainMod, S, togglesplit, # dwindle
    bind = $mainMod, F, fullscreen

    # Move focus with mainMod + arrow keys
    bind = $mainMod, H, movefocus, l
    bind = $mainMod, L, movefocus, r
    bind = $mainMod, K, movefocus, u
    bind = $mainMod, J, movefocus, d

    # Move window with mainMod shift + arrow keys
    bind = $mainMod SHIFT, H, movewindow, l
    bind = $mainMod SHIFT, L, movewindow, r
    bind = $mainMod SHIFT, K, movewindow, u
    bind = $mainMod SHIFT, J, movewindow, d

    # Switch workspaces with mainMod + [0-9]
    bind = $mainMod, 1, workspace, 1
    bind = $mainMod, 2, workspace, 2
    bind = $mainMod, 3, workspace, 3
    bind = $mainMod, 4, workspace, 4
    bind = $mainMod, 5, workspace, 5
    bind = $mainMod, 6, workspace, 6
    bind = $mainMod, 7, workspace, 7
    bind = $mainMod, 8, workspace, 8
    bind = $mainMod, 9, workspace, 9
    bind = $mainMod, 0, workspace, 10

    # Move active window to a workspace with mainMod + SHIFT + [0-9]
    bind = $mainMod SHIFT, 1, movetoworkspace, 1
    bind = $mainMod SHIFT, 2, movetoworkspace, 2
    bind = $mainMod SHIFT, 3, movetoworkspace, 3
    bind = $mainMod SHIFT, 4, movetoworkspace, 4
    bind = $mainMod SHIFT, 5, movetoworkspace, 5
    bind = $mainMod SHIFT, 6, movetoworkspace, 6
    bind = $mainMod SHIFT, 7, movetoworkspace, 7
    bind = $mainMod SHIFT, 8, movetoworkspace, 8
    bind = $mainMod SHIFT, 9, movetoworkspace, 9
    bind = $mainMod SHIFT, 0, movetoworkspace, 10

    bind = ,XF86AudioRaiseVolume,exec,${pkgs.alsa-utils}/bin/amixer sset Master "5"%+
    bind = ,XF86AudioLowerVolume,exec,${pkgs.alsa-utils}/bin/amixer sset Master "5"%-
    bind = ,XF86AudioMute,exec,${pkgs.alsa-utils}/bin/amixer set Master toggle
    bind = ,XF86AudioMicMute,exec,${pkgs.alsa-utils}/bin/amixer set Capture toggle
    bind = $mainMod,F1,exec,audio-switcher
    bind = $mainMod,n,exec,[float;center] /home/jelle/.local/bin/emacs-capture -e "(org-capture)"
    bind = $mainMod SHIFT,n,exec,[float;center] /home/jelle/.local/bin/emacs-capture -e "(org-roam-capture)"

    # Scroll through existing workspaces with mainMod + scroll
    bind = $mainMod, mouse_down, workspace, e+1
    bind = $mainMod, mouse_up, workspace, e-1

    # Screenshot
    bind = $mainMod CTRL,3,exec,grim
    bind = $mainMod CTRL,4,exec,grim -g $(slurp)

    # Move/resize windows with mainMod + LMB/RMB and dragging
    bindm = $mainMod, mouse:272, movewindow
    bindm = $mainMod, mouse:273, resizewindow
    windowrule=float,^(Rofi)$
    windowrule=float,title:^(emacs-capture)$
    windowrule=center,title:^(emacs-capture)$
    windowrule=noblur,title:^(emacs-capture)$
    windowrule=float,title:^(Volume Control)$
    windowrule=float,title:^(Picture-in-Picture)$
    windowrule=pin,title:^(Picture-in-Picture)$
    windowrule=move 75% 75% ,title:^(Picture-in-Picture)$
    windowrule=size 24% 24% ,title:^(Picture-in-Picture)$

    exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
    exec-once=waybar
    exec-once=${pkgs.blueman}/bin/blueman-applet
    exec-once=${pkgs.networkmanagerapplet}/bin/nm-applet
    exec-once=${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1
    exec-once=${pkgs.swayidle}/bin/swayidle -w timeout 300 '${lockCommand}' timeout 600 'hyprctl dispatch dpms off' before-sleep '${lockCommand}'
    exec-once=hyprctl setcursor macOS 24
  '';
in {
    xdg.configFile."hypr/hyprland.conf".text = hyprlandConf;
    home.pointerCursor = {
        gtk.enable = true;
        # x11.enable = true;
        package = pkgs.apple-cursor;
        name = "macOS";
        size = 24;
    };

    gtk = {
        enable = true;

        theme = {
        package = pkgs.flat-remix-gtk;
        name = "Flat-Remix-GTK-Grey-Darkest";
        };

        iconTheme = {
        package = pkgs.adwaita-icon-theme;
        name = "Adwaita";
        };

        font = {
        name = "Sans";
        size = 11;
        };
    };
}
