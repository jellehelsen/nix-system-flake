{ config, lib, pkgs, system, locker, ... }:

{
  imports = [ ../waybar ];

  programs.hyprland = {
    enable = true;
    # enableNvidiaPatches = true;
    xwayland = {
      enable = true;
      # hidpi = false;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      locker
      egl-wayland
      cmake
      swayidle
      wl-clipboard
      wlr-randr
      slurp
      grim
      hyprland-protocols
      xdg-desktop-portal-hyprland
      hyprcursor
      polkit_gnome
    ];

    # variables = {
    #   #WLR_NO_HARDWARE_CURSORS="1";         # Possible variables needed in vm
    #   #WLR_RENDERER_ALLOW_SOFTWARE="1";
    #   XDG_CURRENT_DESKTOP="Hyprland";
    #   XDG_SESSION_TYPE="wayland";
    #   XDG_SESSION_DESKTOP="Hyprland";
    # };

    # sessionVariables = {
    #   QT_QPA_PLATFORM = "wayland";
    #   QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";

    #   GDK_BACKEND = "wayland";
    #   WLR_NO_HARDWARE_CURSORS = "1";
    #   MOZ_ENABLE_WAYLAND = "1";
    # };

  };
  security.pam.services.swaylock = {
    text = ''
      auth include login
    '';
  };
}
