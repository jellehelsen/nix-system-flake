{ config, lib, pkgs, ... }:

{
  programs.borgmatic = {
    enable = true;
    backups = {
      home = {
        location = {
          sourceDirectories = [ "/home/jelle" ];
          repositories = [ "ssh://192.168.1.20/data/backups" ];
          excludeHomeManagerSymlinks = true;
          extraConfig = {
            exclude_patterns =
              [ "*.qcow2" "*.iso" "*.pyc" "*/target" "~/Downloads" ];
            exclude_caches = true;
          };
        };
        storage = {
          encryptionPasscommand = "bw get password borg";
          extraConfig = { archive_name_format = "{hostname}-{user}-{utcnow}"; };
        };
        retention = {
          keepDaily = 7;
          keepWeekly = 4;
          keepMonthly = 6;
        };
      };
    };
  };
}
