{ config, lib, pkgs, ... }:
{
  xdg.configFile."polybar/config.ini".source = ./config;
  xdg.configFile."polybar/scripts/pavolume.sh".source = ./pavolume.sh;
  xdg.configFile."polybar/scripts/pavolume.sh".executable = true;
  xdg.configFile."polybar/scripts/spotify.sh".source = ./spotify.sh;
  xdg.configFile."polybar/scripts/spotify.sh".executable = true;
}
