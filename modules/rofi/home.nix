{ config, lib, pkgs, ... }:

{
  programs.rofi = {
    enable = true;
    theme = "rounded-purple-dark";
    package = pkgs.rofi-wayland;
    cycle = true;
    location = "center";
    terminal = "alacritty";
    # plugins = [ pkgs.rofi-calc ];
    extraConfig = {
      modi = "combi,ssh,filebrowser,window,drun,run,keys";
      case-sensitive = false;
      normalize-match = false;
      sidebar-mode = false;
      window-format = "{w} - {c}\\n{t}";
      window-thumbnail = false;
      kb-accept-entry = "Return,KP_Enter";
      kb-row-down = "Down,Control+j";
      kb-row-up = "Up,Control+k";
      kb-mode-next = "Control+n";
      kb-mode-previous = "Control+p";
      kb-remove-to-eol = "";

      display-drun = "$";
      display-run = "#";
      display-window = "<U+F2D2>";
      display-windowcd = "缾";
      display-ssh = ":$";
      display-combi = ">";
      # display-calc = "<U+F5EB>";
      display-keys = "<U+F11C>";
      display-filebrowser = "<U+F07C>";
    };
  };
  home.file = {
    ".config/rofi/themes/rounded-common.rasi" = {
      source = ./rounded-common.rasi;
    };
    ".config/rofi/themes/rounded-purple-dark.rasi" = {
      source = ./rounded-purple-dark.rasi;
    };
    ".config/rofi/exit.sh" = {
      source = ./exit.sh;
      executable = true;
    };
  };
}
