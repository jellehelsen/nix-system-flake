#!/usr/bin/env sh

lock() {
    hyprlock
}


result=$(echo Lock:Sleep:Suspend:Logout:Reboot:Shutdown | rofi -dmenu -i -sep : -p Quit?)
case $result in
    Lock)  lock ;;
    Sleep) lock && hyprctl dispatch dpms off ;;
    Suspend) lock && systemctl suspend ;;
    Logout) hyprctl dispatch exit ;;
    Reboot) reboot ;;
    Shutdown) shutdown now ;;
esac
