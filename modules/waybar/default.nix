{ config, lib, pkgs, ... }:

{
  environment.systemPackages = [ pkgs.waybar ];
}
