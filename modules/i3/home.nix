{ config, lib, pkgs, ... }:
let
      purple = "#8d62a9";
      blue = "#6790eb";
      background = "#2f343f";
      foreground = "#fefefe";
      alert = "#bd2c40";
      modifier = "Mod4";
      workspace1 = "1 ";
      workspace2 = "2 ";
      workspace3 = "3 ";
      workspace4 = "4 ";
      workspace5 = "5 ";
      workspace6 = "6 ";
      workspace7 = "7 ";
      workspace8 = "8 ";
      workspace9 = "9 ";
      workspace10 = "10 ";
      systemmode = "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown";
      polybar = pkgs.polybar.override {
        i3Support = true;
        alsaSupport = true;
        iwSupport = true;
        githubSupport = true;
      };
      artwork = pkgs.stdenv.mkDerivation {
        name = "artwork";
        src = pkgs.fetchgit {
          url = "https://github.com/NixOS/nixos-artwork.git";
          sparseCheckout = [
            "wallpapers"
          ];
          hash = "sha256-aiEFW6zSa/ltVxe8hBQ2o2s+PLJvwBi4dzjpYvWjoWo=";
        };
        buildPhase = "echo hello";
        installPhase = ''
          mkdir -p $out
          cp -r wallpapers $out/
        '';
      };

      dimScreenPkg = pkgs.writeShellScriptBin "dimscreen" ''
monitors=$(xrandr --listmonitors | awk '/^ [0-9]/{print $4}')

reset() {
    for monitor in $monitors; do
        xrandr --output $monitor --brightness 1
    done
}

dim() {
    for monitor in $monitors; do
        xrandr --output $monitor --brightness 0.7
    done
}


trap 'exit 0' TERM INT
trap "reset; kill %%" EXIT
dim
sleep 2147483647 &
wait
'';
      polybarScriptPkg = pkgs.writeShellScriptBin "polybar-start" ''
# Terminate already running bar instances
set -x
${pkgs.procps}/bin/pgrep -f ${polybar}/bin/polybar | xargs kill
sleep 1
# Wait until the processes have been shut down
while not ${pkgs.procps}/bin/pgrep -f ${polybar}/bin/polybar >/dev/null; do sleep 1; done

for m in $(${polybar}/bin/polybar --list-monitors | cut -d":" -f1); do
  echo "starting $m"
	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m ${polybar}/bin/polybar --reload mainbar-i3 &
done
'';
      startupScriptPkg = pkgs.writeShellScriptBin "i3-startup" ''
xset s 180 120
${pkgs.xss-lock}/bin/xss-lock -n "${dimScreenPkg}/bin/dimscreen" -l -- ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p &
# setxkbmap -option compose:ralt

# exec_always --no-startup-id feh --bg-scale /home/jelle/.wallpaper.jpg
~/.screenlayout/default.sh
# /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
${pkgs.nitrogen}/bin/nitrogen --head=0 --set-scaled ~/Pictures/wallpaper4.jpg
${pkgs.picom}/bin/picom -b
# $HOME/.config/polybar/launch.sh
# pa-applet &
# nm-applet &
blueman-applet
${polybarScriptPkg}/bin/polybar-start
'';
in
{

  xsession.windowManager.i3 = {
    enable = true;
    config = {
      modifier = "${modifier}";
      terminal = "${pkgs.alacritty}/bin/alacritty";
      assigns = {
        "${workspace1}" = [
          { class = "^Firefox$"; }
          { class = "brave"; }
        ];
        "${workspace3}" = [{ class = "Emacs"; }];
        "${workspace9}" = [{ class = "teams-for-linux"; } { class = "discord"; }];
        "${workspace10}" = [{ class = "Spotify"; }];
      };
      workspaceOutputAssign = [
        {workspace = "${workspace1}"; output = "DP-1";}
        {workspace = "${workspace2}"; output = "DP-1";}
        {workspace = "${workspace3}"; output = "DP-1";}
        {workspace = "${workspace4}"; output = "DP-1";}
        {workspace = "${workspace5}"; output = "DP-1";}
        {workspace = "${workspace9}"; output = "eDP-1";}
        {workspace = "${workspace10}"; output = "eDP-1";}
      ];
      keybindings = lib.mkOptionDefault {
        "${modifier}+Shift+q" = "kill";
        "Ctrl+space" = "exec rofi -show drun";

        "${modifier}+h" = "focus left";
        "${modifier}+j" = "focus down";
        "${modifier}+k" = "focus up";
        "${modifier}+l" = "focus right";

        "${modifier}+Shift+h" = "move left";
        "${modifier}+Shift+j" = "move down";
        "${modifier}+Shift+k" = "move up";
        "${modifier}+Shift+l" = "move right";

        "${modifier}+1" = "workspace ${workspace1}";
        "${modifier}+2" = "workspace ${workspace2}";
        "${modifier}+3" = "workspace ${workspace3}";
        "${modifier}+4" = "workspace ${workspace4}";
        "${modifier}+5" = "workspace ${workspace5}";
        "${modifier}+6" = "workspace ${workspace6}";
        "${modifier}+7" = "workspace ${workspace7}";
        "${modifier}+8" = "workspace ${workspace8}";
        "${modifier}+9" = "workspace ${workspace9}";
        "${modifier}+0" = "workspace ${workspace10}";

        "${modifier}+Shift+1"  = "move container to workspace ${workspace1}";
        "${modifier}+Shift+2"  = "move container to workspace ${workspace2}";
        "${modifier}+Shift+3"  = "move container to workspace ${workspace3}";
        "${modifier}+Shift+4"  = "move container to workspace ${workspace4}";
        "${modifier}+Shift+5"  = "move container to workspace ${workspace5}";
        "${modifier}+Shift+6"  = "move container to workspace ${workspace6}";
        "${modifier}+Shift+7"  = "move container to workspace ${workspace7}";
        "${modifier}+Shift+8"  = "move container to workspace ${workspace8}";
        "${modifier}+Shift+9"  = "move container to workspace ${workspace9}";
        "${modifier}+Shift+0"  = "move container to workspace ${workspace10}";

        "XF86AudioRaiseVolume" = "exec --no-startup-id amixer sset Master \"5\"%+";
        "XF86AudioLowerVolume" = "exec --no-startup-id amixer sset Master \"5\"%-";
        "XF86AudioMute"        = "exec --no-startup-id amixer set Master toggle";
        "XF86AudioMicMute"     = "exec --no-startup-id amixer set Capture toggle";

        "${modifier}+F1" = "exec ~/.screenlayout/single.sh && ${polybarScriptPkg}/bin/polybar-start";
        "${modifier}+F2" = "exec ~/.screenlayout/default.sh && ${polybarScriptPkg}/bin/polybar-start";
        "${modifier}+F4" = "exec audio-switcher";
        "${modifier}+Shift+e"  = "mode \"${systemmode}\"";
      };

      startup = [
        {command = "${startupScriptPkg}/bin/i3-startup";}
      ];
      menu = "rofi -show drun";

      gaps = {
        inner = 6;
        outer = 12;
      };
      bars = [];
      modes = {
          resize = {
            "h" = "resize shrink width 10 px or 10 ppt";
            "j" = "resize grow height 10 px or 10 ppt";
            "k" = "resize shrink height 10 px or 10 ppt";
            "l" = "resize grow width 10 px or 10 ppt";
            "Escape" = "mode default";
            "Return" = "mode default";
          };
          "${systemmode}" = {
            "l" = "exec --no-startup-id ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p, mode \"default\"";
            "e" = "exec --no-startup-id i3-msg exit, mode \"default\"";
            "s" = "exec --no-startup-id ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p && systemctl suspend, mode \"default\"";
            "h" = "exec --no-startup-id ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p && systemctl hibernate, mode \"default\"";
            "r" = "exec --no-startup-id ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p && systemctl reboot, mode \"default\"";
            "Shift+s" = "exec --no-startup-id ${pkgs.i3lock-fancy}/bin/i3lock-fancy -p && systemctl poweroff, mode \"default\"";
            "Escape" = "mode default";
            "Return" = "mode default";
          };
      };
      colors.focused = {
        border = purple;
        childBorder = purple;
        background = purple;
        text = "#f3f4f5";
        indicator = "#00aa00";
      };
      colors.unfocused = {
        border = background;
        childBorder = background;
        background = background;
        text = "#676e7d";
        indicator = "#00aa00";
      };
      colors.focusedInactive = {
        border = background;
        childBorder = background;
        background = background;
        text = "#676e7d";
        indicator = "#00aa00";
      };
      colors.urgent = {
        border = alert;
        childBorder = alert;
        background = alert;
        text = "#f3f4f5";
        indicator = "#00aa00";
      };
    };
  };

}
