{
  description = "A very basic flake";

  outputs = { self, nixpkgs, ... }:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    in
      {
        packages = forAllSystems (system:
          let
            depsPath = with pkgs.${system}; nixpkgs.lib.makeBinPath [
              coreutils
              grim
              gawk
              swaylock
              imagemagick
              fontconfig
            ];
          in
          rec{
            locker = pkgs.${system}.stdenv.mkDerivation {
              name = "hyprlock";
              src = ./.;
              buildInputs = [ pkgs.${system}.makeWrapper ];
              installPhase = ''
                runHook preInstall

                mkdir -p $out/bin
                mkdir -p $out/share
                substitute locker.sh $out/bin/hyprlock --replace white.svg $out/share/white.svg
                chmod a+x $out/bin/hyprlock
                cp white.svg $out/share/

                runHook postInstall
              '';

              postInstall = ''
                wrapProgram $out/bin/hyprlock \
                  --prefix PATH : "${depsPath}"
              '';
            };
            default = locker;
          }
        );

        devShells = forAllSystems (system: {
          default = pkgs.${system}.mkShellNoCC {
            packages = with pkgs.${system}; [
              imagemagick
              grim
              swaylock
              makeWrapper
            ];
          };
        });
      };
}
