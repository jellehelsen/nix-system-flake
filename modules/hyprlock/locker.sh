#!/usr/bin/env sh

text="Type password to unlock"
font=$(convert -list font | awk "{ a[NR] = \$2 } /family: $(fc-match sans -f "%{family}\n")/ { print a[NR-1]; exit }")

monitors=$(hyprctl monitors | grep Monitor | awk '{print $2}')

param=("-f" "--inside-color=ffffff1c" "--ring-color=ffffff3e" \
    "--line-color=ffffff00" "--key-hl-color=00000080" "--ring-ver-color=00000000" \
    "--separator-color=22222260" "--inside-ver-color=0000001c" \
    "--ring-clear-color=ff994430" "--inside-clear-color=ff994400" \
    "--ring-wrong-color=00000055" "--inside-wrong-color=0000001c" "--text-ver-color=00000000" \
    "--text-wrong-color=00000000" "--text-caps-lock-color=00000000" "--text-clear-color=00000000" \
    "--line-clear-color=00000000" "--line-wrong-color=00000000" "--line-ver-color=00000000" "--text-color=DB3300FF")

lock_cmd="swaylock ${param[@]}"

tmpdir=$(mktemp -d)
echo ${tmpdir}

for monitor in $monitors; do
    grim -o $monitor ${tmpdir}/${monitor}.png
    convert -background none ${tmpdir}/${monitor}.png \
        -filter Gaussian -resize 20% \
        -define "filter:sigma=1.5" \
        -resize 500.5% -font ${font} -pointsize 26 \
        -gravity center -fill white \
        -annotate +0+160 "${text}" white.svg \
        -gravity center -composite ${tmpdir}/${monitor}.png
    lock_cmd+=" -i ${monitor}:${tmpdir}/${monitor}.png"
done

eval ${lock_cmd}
rm -rf ${tmpdir}
