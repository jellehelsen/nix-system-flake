{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware.nix
    ./programs.nix
    ./services.nix
  ];
}
