{ config, lib, pkgs, ... }:

{
  programs.xwayland.enable = true;
  programs.fish.enable = true;
  programs.nm-applet.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
