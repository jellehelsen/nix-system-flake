{ config, lib, pkgs, ... }:

{
  services.blueman.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Configure keymap in X11
  services.xserver = {
    enable = true;
    videoDrivers = ["nvidia"];
    xkb = {
      layout = "us";
      variant = "";
    };
    displayManager.gdm.enable = true;
    # desktopManager.gnome.enable = true;
    windowManager.hypr.enable = true;
    windowManager.qtile.enable = true;
    windowManager.i3.enable = true;
    # windowManager.qtile.package = pkgs.stable.qtile;
    # windowManager.qtile.backend = "x11";
  };

  services.displayManager.defaultSession = "hyprland";
  # Enable CUPS to print documents.
  services.printing.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  services.gvfs.enable = true;
  services.udisks2.enable = true;
  services.devmon.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.avahi.enable = true;
  services.avahi.publish.enable = true;
  services.avahi.publish.userServices = true;
  services.zerotierone = {
    enable = true;
    joinNetworks = ["a0cbf4b62a443fdf"];
  };
}
