{ config, lib, pkgs, user, audio-switcher, nixpkgs-stable, ... }:
let
  stable = import nixpkgs-stable {
    system = pkgs.system;
    # Uncomment this if you need an unfree package from stable.
    config.allowUnfree = true;
  };
in
{
  imports = [
    (import ../modules/hyprland/home.nix)
    (import ../modules/waybar/home.nix)
    (import ../modules/rofi/home.nix)
    (import ../modules/dunst/home.nix)
    (import ../modules/borgmatic/home.nix)
    (import ../modules/kubernetes/home.nix)
    (import ../modules/i3/home.nix)
    (import ../modules/polybar/home.nix)
  ];
  home = {
    stateVersion = "24.11";
    username = "jelle";
    homeDirectory = "/home/jelle";

    packages = with pkgs; [
      htop
      feh
      mpv
      pavucontrol
      google-chrome
      kitty
      pcmanfm
      bitwarden-cli
      brave
      spotify
      silver-searcher
      stable.azure-cli
      teams-for-linux
      davmail
      jq
      isync
      ispell
      graphviz
      fortune
      cowsay
      lolcat
      audio-switcher
      font-manager
      docker-compose
      discord
      # spotify-tui
      playerctl
      stable.freecad
      pre-commit
      kicad
      prusa-slicer
      alacritty-theme
      pycritty
      pulseaudioFull
      xsel
      cmake
      libtool
      commitizen
      pipx
      devbox
    ];
    file = {
      ".doom.d" = {
        source = ./doom.d;
        recursive = true;
      };
      # ".config/nvim" = {
      #   source = pkgs.fetchFromGitHub {
      #     owner = "doom-neovim";
      #     repo = "doom-nvim";
      #     rev = "main";
      #     sha256 = "sha256-Y3BO04oMDwjfYoIZO+oZEzmEHVlkuLZUTwnKNjeaYo4=";
      #   };
      # };
      ".local/bin/emacs-capture" = {
        text = ''
          #!/bin/sh
          emacsclient -c -F '((name . "emacs-capture"))' $@
        '';
        executable = true;
      };
    };
  };

  services = {
    emacs = {
      enable = true;
      defaultEditor = true;
      client.enable = true;
      package = pkgs.emacs29;
    };
    blueman-applet.enable = true;
    network-manager-applet.enable = true;
    gpg-agent = {
      enable = true;
      enableFishIntegration = true;
      enableSshSupport = true;
    };
    spotifyd = {
      enable = true;
      package = (pkgs.spotifyd.override { withMpris = true; });
      settings = {
        global = {
          username = "jelle.helsen@hcode.be";
          password_cmd = "${pkgs.pass}/bin/pass show spotifyd";
          use_mpris = true;
          dbus_type = "session";
        };
      };
    };
  };

  programs = {
    pyenv.enable = true;
    pyenv.enableFishIntegration = true;
    pyenv.rootDirectory = "${config.xdg.dataHome}/pyenv";

    home-manager.enable = true;
    password-store = {
      enable = true;
      settings = {
        PASSWORD_STORE_DIR = "$HOME/.local/share/password-store";
      };
    };

    emacs = {
      enable = true;
      package = pkgs.emacs29;
    };
    nushell.enable = true;

    neovim = {
      enable = true;
      vimAlias = true;
      withPython3 = true;
    };

    ssh = {
      enable = true;
      forwardAgent = true;
      matchBlocks = {
        miner.hostname = "192.168.1.20";
        "vmm-tunnel" = {
          hostname = "127.0.0.1";
          port = 2201;
          dynamicForwards = [ { port = 2280; } ];
          localForwards = [
            {
              bind.port = 2288;
              host.address = "127.0.0.1";
              host.port = 8888;
            }
          ];
          extraOptions = {
            LogLevel = "QUIET";
            StrictHostKeyChecking = "no";
            UserKnownHostsFile = "/dev/null";
          };
        };
        "github.com" = {
          hostname = "github.com";
          user = "git";
          identityFile = "~/.ssh/id_ed25519";
        };
        "bitbucket.org" = {
          hostname = "bitbucket.org";
          user = "git";
          identityFile = "~/.ssh/id_rsa";
        };
      };
    };

    alacritty = {
      enable = true;
      settings = {
        general.import = ["${pkgs.alacritty-theme}/dracula.toml"];
      };
    };

    fish = {
      enable = true;
      functions = { fish_greeting = "fortune -as | cowsay -f tux | lolcat"; };
      interactiveShellInit = ''
      fish_vi_key_bindings
      if test $TERM = "alacritty"
        eval (${pkgs.zellij}/bin/zellij setup --generate-auto-start fish | string collect)
      end
    '';
      shellAliases = {
        gst = "git status";
        gc = "git commit";
        gco = "git checkout";
        gcb = "git checkout -b";
        gcd = "git checkout develop";
        gcm = "git checkout main";
        gl = "git log";
        gp = "git push";
      };
      plugins = [
        {
          name = "z";
          src = pkgs.fetchFromGitHub {
            owner = "jethrokuan";
            repo = "z";
            rev = "master";
            sha256 = "sha256-+FUBM7CodtZrYKqU542fQD+ZDGrd2438trKM0tIESs0=";
          };
        }
        {
          name = "pisces";
          src = pkgs.fetchFromGitHub {
            owner = "laughedelic";
            repo = "pisces";
            rev = "master";
            sha256 = "sha256-Oou2IeNNAqR00ZT3bss/DbhrJjGeMsn9dBBYhgdafBw=";
          };
        }
        {
          name = "fish-color-scheme-switcher";
          src = pkgs.fetchFromGitHub {
            owner = "h-matsuo";
            repo = "fish-color-scheme-switcher";
            rev = "master";
            sha256 = "sha256-9VOgPxCNfuyy9DlicaN6W2EQ6OQFq/z8oSx1l9RitP4=";
          };
        }
      ];
    };

    bash = {
      enable = true;
      shellAliases = {
        gst = "git status";
        gc = "git commit";
        gco = "git checkout";
        gcb = "git checkout -b";
        gcd = "git checkout develop";
        gcm = "git checkout main";
        gl = "git log";
        gp = "git push";
      };
    };

    starship =
      let
        flavour = "mocha";
      in
        {
          enable = true;
          settings = {
            # Other config here
            format = "$all"; # Remove this line to disable the default prompt format
            palette = "catppuccin_${flavour}";
            character = {
              success_symbol = "[[♥](green) ❯](green)";
              error_symbol = "[❯](red)";
              vimcmd_symbol = "[❮](green)";
            };
          } // builtins.fromTOML (builtins.readFile
            (pkgs.fetchFromGitHub
              {
                owner = "catppuccin";
                repo = "starship";
                rev = "3c4749512e7d552adf48e75e5182a271392ab176"; # Replace with the latest commit hash
                sha256 = "sha256-t/Hmd2dzBn0AbLUlbL8CBt19/we8spY5nMP0Z+VPMXA=";
              } + /themes/${flavour}.toml));
        };

    firefox.enable = true;

    git = {
      enable = true;
      userName = "Jelle Helsen";
      userEmail = "jelle.helsen@hcode.be";
      delta.enable = true;
      extraConfig = {
        github = {
          user = "jellehelsen";
        };
      };
    };

    direnv.enable = true;

    # exa = {
    #   enable = true;
    #   enableAliases = true;
    #   git = true;
    #   icons = true;
    # };

    zellij = {
      enable = true;
      # enableFishIntegration = true;
    };
  };
  systemd.user.services.spotifyd.Unit.After = "graphical-session.target";
  systemd.user.services.spotifyd.Install.WantedBy = lib.mkForce ["graphical-session.target"];
  xdg.desktopEntries = {
    "Org Protocol handler" = {
      type = "Application";
      name = "Org Protocol Scheme Handler";
      exec = "/home/jelle/.local/bin/emacs-capture -- %u";
      startupNotify = false;
      mimeType = [ "x-scheme-handler/org-protocol" ];
    };
  };
}
