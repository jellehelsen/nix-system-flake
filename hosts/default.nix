{ nixpkgs, home-manager, audio-switcher, locker, rust-findutils, nixpkgs-stable, ... }:
let
  modules = [
    ./configuration.nix
  ];

in {
  nixos = nixpkgs.lib.nixosSystem {
    inherit modules;
    specialArgs = {
      inherit home-manager audio-switcher locker rust-findutils nixpkgs-stable;
      host = { hostname = "nixos"; };
    };
  };
  beastnix = nixpkgs.lib.nixosSystem {
    inherit modules;
    specialArgs = {
      inherit home-manager audio-switcher locker rust-findutils nixpkgs-stable;
      host = { hostname = "beastnix"; };
    };
  };
}
