{ config, lib, pkgs, host, hyprland, home-manager, audio-switcher, locker, rust-findutils, nixpkgs-stable, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./config
    # hyprland.nixosModules.default
    ../modules/hyprland
    home-manager.nixosModules.home-manager
    {
      home-manager.useGlobalPkgs = true;
      home-manager.useUserPackages = true;
      home-manager.extraSpecialArgs = {
        inherit audio-switcher locker nixpkgs-stable;
      };
      home-manager.users.jelle = { imports = [ ./home.nix ]; };
    }
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 42;
  # boot.loader.efi.canTouchEfiVariables = true;
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  # boot.loader.grub.theme = pkgs.sleek-grub-theme;

  networking = {
    hostName = host.hostname;
    networkmanager.enable = true;
  };

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    # Add any missing dynamic libraries for unpackaged programs
    # here, NOT in environment.systemPackages
    libvirt-glib
    lxc
    zlib
    libffi
    readline
    ncurses
  ];

  environment.sessionVariables = {
    # pyenv flags to be able to install Python
    CPPFLAGS="-I${pkgs.zlib.dev}/include -I${pkgs.libffi.dev}/include -I${pkgs.readline.dev}/include -I${pkgs.bzip2.dev}/include -I${pkgs.openssl.dev}/include -I${pkgs.ncurses.dev}/include";
    CXXFLAGS="-I${pkgs.zlib.dev}/include -I${pkgs.libffi.dev}/include -I${pkgs.readline.dev}/include -I${pkgs.bzip2.dev}/include -I${pkgs.openssl.dev}/include -I${pkgs.ncurses.dev}/include";
    CFLAGS="-I${pkgs.openssl.dev}/include -I${pkgs.ncurses.dev}/include";
    LDFLAGS="-L${pkgs.zlib.out}/lib -L${pkgs.libffi.out}/lib -L${pkgs.readline.out}/lib -L${pkgs.bzip2.out}/lib -L${pkgs.openssl.out}/lib -L${pkgs.ncurses.out}/lib";
    CONFIGURE_OPTS="-with-openssl=${pkgs.openssl.dev}";
    PYENV_VIRTUALENV_DISABLE_PROMPT="1";
  };

  # Set your time zone.
  time.timeZone = "Europe/Brussels";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "nl_BE.UTF-8";
    };
  };


  # Enable sound with pipewire.
  # sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.sudo = {
    enable = true;
    package = pkgs.sudo.override { withInsults = true; };
  };
  security.rtkit.enable = true;
  security.polkit.enable = true;


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jelle = {
    isNormalUser = true;
    description = "Jelle Helsen";
    extraGroups = [ "networkmanager" "wheel" "input" "lxd" "docker" "storage" "libvirtd" ];
    hashedPassword = "$y$j9T$4gXuanwMBUZ/H5ZFpGmZy1$wqPkyuLWBYVhFl6LP4Ss0WRuWhcqMr9LfvDdSxOZou8";
    shell = pkgs.fish;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    ripgrep
    fd
    coreutils
    bat
    gccgo
    gnumake
    # rust-findutils
    gcc
    gnumake
    zlib
    libffi
    readline
    bzip2
    openssl
    ncurses
  ];
  environment.localBinInPath = true;


  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?

  nix = {
    # package = pkgs.nixFlakes;
    extraOptions = "experimental-features = nix-command flakes";
  };
  # system.userActivationScripts = {
  #   doomEmacs = {
  #     text = ''
  #       source ${config.system.build.setEnvironment}
  #       EMACS="$HOME/.config/emacs"

  #       if [ ! -d "$EMACS" ]; then
  #         ${pkgs.git}/bin/git clone https://github.com/doomemacs/doomemacs.git $EMACS
  #         yes | $EMACS/bin/doom install
  #         systemctl --user restart emacs
  #       else
  #         $EMACS/bin/doom sync
  #       fi
  #     '';
  #   };
  # };
  fonts = {
    packages = with pkgs; [
      font-awesome
      carlito
      vegur
      corefonts
      nerdfonts
      roboto
      fira
      fira-mono
      fira-code
      fira-code-symbols
      fira-code-nerdfont
    ];
    fontDir.enable = true;
  };


  virtualisation = {
    lxd.enable = true;
    lxc.lxcfs.enable = true;
    libvirtd.enable = true;
    podman = {
      enable = true;
      dockerSocket.enable = false;
    };
    docker = {
      enable = true;
      # enableNvidia = true;
    };
    containers.registries.insecure = ["localhost:5000"];
    # containers.cdi.dynamic.nvidia.enable = true;
  };
  services.gvfs.enable = true;

  # To unbreak teams
  nixpkgs.config.permittedInsecurePackages = [
    "electron-24.8.6"
  ];
  # systemd.enableUnifiedCgroupHierarchy = lib.mkForce true;
}

