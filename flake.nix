{
  description = "My system configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    home-manager = { # User Package Management
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # hyprland = { # Official Hyprland flake
    #   url =
    #     "github:vaxerski/Hyprland/v0.32.3"; # Add "hyprland.nixosModules.default" to the host modules
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    audio-switcher = {
      url = "gitlab:jellehelsen/audio-switcher";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    locker = {
      url = "./modules/hyprlock";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-findutils = {
      url = "github:jellehelsen/findutils/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, audio-switcher, locker, rust-findutils, nixpkgs-stable }:
    let
      system = "x86_64-linux";
    in {
      nixosConfigurations = (import ./hosts {
        inherit nixpkgs home-manager nixpkgs-stable;
        audio-switcher = audio-switcher.packages.${system}.default;
        locker = locker.packages.${system}.default;
        rust-findutils = rust-findutils.packages.${system}.default;
      });
    };
}
